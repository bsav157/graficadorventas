var datos = [];

function registrarVentas(){
    var meses = 12;
    var formulario = document.getElementById("formulario");
    var form = "<form class='form-group'><div class='row'>";
    for(i=0;i<meses;i++){
        form += "<div class='col-md-6'><label> Mes: " + (i+1) +  "</label>";
        form +="<input class='form-control type='number' min='0' id='mes' name='mes' required/></div>";
    }
    form +="</div><br><br><div class='text-center'><button type='button' class='btn btn-danger txt-center' onclick='graficar()'>Graficar</button></div>";
    form +="</form>";
    formulario.innerHTML=form;
}

function graficar(){
    datos = [];
    var ventas = document.getElementsByName("mes");
    var anio = document.getElementById("anio").value;
    var seleccion = document.getElementById("periodo");
    var periodo = [seleccion.value, seleccion.options[seleccion.value].text];

    var ret = validarDatos(anio, periodo[0], ventas);
    if(ret!=''){
        alert(ret);
    } else {
        procesarVentas(periodo, anio, ventas);
        drawChart(anio, periodo);
        generarTabla(periodo);
    }

}

function validarDatos(anio, periodo, ventas){
    var ret = '';
    if (anio > 2020 || anio == '')
        ret = "Año ingresado es incorrecto, digite nuevamente";
    else if (periodo == '0')
        ret = "Debe seleccionar un periodo de análisis de datos";
    else {
        for (var i = 0; i < ventas.length; i++) {
            if (ventas[i].value < 0 || ventas[i].value == '') {
                ret = "Los datos ingresados sobre las ventas son incorrectos, verifique de nuevo";
                i = ventas.length;
            }
        }
    }
    return ret;
}

function drawChart(anio, periodo) {
    alert("Se va a graficar los datos de manera " + periodo[1] + " del año " + anio);
    var data = new google.visualization.arrayToDataTable(datos);
    var options = {
        chart: {
            title: 'Informe' + periodo[1] + ' de ventas',
            subtitle: 'Año: ' + anio,
        },
        bars: 'vertical'
    };
    var chart = new google.charts.Bar(document.getElementById('graficar'));
    chart.draw(data, google.charts.Bar.convertOptions(options));
}

function procesarVentas(periodo, anio, ventas) {
    datos[0] = ["Informe de Ventas " + periodo[1] + " - Año " + anio, "Ventas"];
    var per = periodo[0];
    if(periodo[0]==5){
        per++;
    }
    var aux = 1;
    var j = 1;
    var suma = 0;
    for (var i = 0; i < 12; i++) {
        suma += parseInt(ventas[i].value, 10);
        if(aux == periodo[0]){
            datos[j] = [j , suma];
            suma = 0;
            aux = 1;
            j++;
        } else {
            aux++;
        }      
    }
}

function generarTabla(periodo){
    var tabla = document.getElementById("tabla");
    var aux = "<table class='table table-bordered table-striped table-bordered table-hover>";
    aux += "<thead><tr><th colspan='3'>" + datos[0][0] + "</th></tr></thead>";
    aux += "<tbody><tr> <th>" + periodo[1] + "</th>";
    aux += "<th> Valor </th> </tr>";
    
    for (var i = 1; i < datos.length; i++) {
        aux += "<tr> <th>" + (i) + "</th>";
        aux += "<th> $" + parseInt(datos[i][1], 10) + "</th> </tr>";
    }

    aux += "</tbody></table>";
    tabla.innerHTML = aux;
}